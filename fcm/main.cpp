#include <iostream>
#include <fstream>
#include <map>

using namespace std;

//const size_t Buffersize = 2;

int main(int argc, char *argv[]) {

    if (argc<3){
        cout << "Faltam argumentos" << endl << "./[nome_programa] [nome_ficheiro_texto] [alfa]" << endl;
        return 0;
    }

    int charCount = 0; // Contador de caracteres para calcular entropia
    string ficheiro = argv[1]; // Nome do ficheiro a ler
//    string ficheiro = "../../ficheiro.txt"; // Nome do ficheiro a ler

    /* HASH MAP HERE */
//    map<char, int> entropia;
    map<string, int> entropia;

/*    for (int i=32; i < 127; i++){
//        entropia.insert(i, 0);
        entropia[i] = 0;
    }*/

    cout << "A criar mapa" << endl;

    ifstream ficheiroLeitura; // Instancia file stream do ficheiro a ler
    ficheiroLeitura.open(ficheiro); //

    if (ficheiroLeitura) {
        cout << "Ficheiro encontrado" << endl;
    } else {
        cout << "Ficheiro não encontrado" << endl;
        return 0;
    }

    cout << "Ficheiro: " << ficheiro << endl;
    cout << "A ler ficheiro" << endl;

//    char caracter[Buffersize+1]; // Caracter a recolher do ficheiro
    char caracter; // Caracter a recolher do ficheiro
    string caracteres; // String de caracteres para calcular probabilidade de frequencias relativas
    while(!ficheiroLeitura.eof()){
        caracteres = caracter; // Vai buscar ultimo caracter utilizado
        ficheiroLeitura.get(caracter);
        charCount++;
        caracteres += caracter;
        /* Obtem segundo caracter */
        if (charCount < 2){
            ficheiroLeitura.get(caracter);
            charCount++;
            caracteres += caracter;
        }

//        ficheiroLeitura.read(caracter, Buffersize);

        /* INCREASE HASH MAP COUNT */
        entropia[caracteres]++;

        /*if (entropia.find(caracter)){

        } else {
            entropia.insert(pair<char, int>(caracter, 1));
        }*/

    }

    ficheiroLeitura.close();

    cout << "Entropia criada" << endl << "A imprimir entropia" << endl;

    /* PRINT HASH MAP COUNTS */
    int alfa = atoi(argv[2]); // Alfa para calculo da entropia de caracteres não presentes no texto
//    int alfa = 10; // Alfa para calculo da entropia de caracteres não presentes no texto
    cout << "Alfa: " << alfa << endl;
    for(auto elem : entropia)
    {
        cout << elem.first << " " << elem.second << " " << (float) (elem.second + alfa) / (charCount + alfa) << "\n";
    }

    return 0;
}